export interface UsuarioI {
  usuario: string;
  password: string;
}

export interface UsuarioDataI {
  usuario: string;
  nombre: string;
  apellido: string;
  sexo: string;
}

export const listUsuarios: UsuarioDataI[] = [
  {usuario: 'jperez', nombre: 'Juan', apellido: 'Perez', sexo: 'Masculino'},
  {usuario: 'mgomez', nombre: 'Maria', apellido: 'Gomez', sexo: 'Masculino'},
  {usuario: 'ngarcia', nombre: 'Nilda', apellido: 'Garcia', sexo: 'Femenino'},
  {usuario: 'kliop', nombre: 'Kevin', apellido: 'Liop', sexo: 'Masculino'},
  {usuario: 'hmarino', nombre: 'Hernan', apellido: 'Marino', sexo: 'Masculino'},
  {usuario: 'mmendizabal', nombre: 'Magdiel', apellido: 'Mendizabal', sexo: 'Femenino'}
];
