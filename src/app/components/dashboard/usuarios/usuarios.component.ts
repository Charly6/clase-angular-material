import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { UsuarioDataI } from "src/app/interfaces/usuario.interface";
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { UsuarioService } from 'src/app/services/usuario.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';

@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.css']
})
export class UsuariosComponent implements OnInit, AfterViewInit {

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  listUsuarios: UsuarioDataI[] = [];
  displayedColumns: string[] = ['usuario', 'nombre', 'apellido', 'sexo', 'acciones'];
  dataSource!: MatTableDataSource<any>;

  constructor(private _usuarioService: UsuarioService,
              private _snackbar: MatSnackBar,
              private router: Router,) { }

  ngOnInit(): void {
    this.cargarUsuarios();
  }

  cargarUsuarios(){
    this.listUsuarios = this._usuarioService.getUsuario();
    this.dataSource = new MatTableDataSource(this.listUsuarios);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }
    
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    console.log(filterValue);

    this.dataSource.filter = filterValue.trim().toLowerCase();
    console.log(this.dataSource);
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  eliminarUsuario(usuario: string){
    const opcion = confirm('Estas seguro de eliminar el usuario');

    if(opcion) {
      this._usuarioService.eliminarUsuario(usuario);
      this.cargarUsuarios();

      this._snackbar.open('El usuario fue eliminado con exito', '', {
        duration: 1500,
        horizontalPosition: 'center',
        verticalPosition: 'bottom',
      });
    }
  }

  verUsuario(usuario: string){
    console.log(usuario);
    this.router.navigate(['dashboard/ver-usuario', usuario]);
  }

  modificarUsuario(usuario: string){
    console.log(usuario);
    this.router.navigate(['dashboard/crear-usuario', usuario]);
  }

}
