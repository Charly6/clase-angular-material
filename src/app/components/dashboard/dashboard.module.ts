import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardroutingModule } from "./dashboard-routing.module";
import { DashboardComponent } from './dashboard.component';
import { NavbarComponent } from 'src/app/components/dashboard/navbar/navbar.component';
import { InicioComponent } from './inicio/inicio.component';
import { UsuariosComponent } from './usuarios/usuarios.component';
import { SharedModule } from "../shared/shared.module";
import { ReportesComponent } from './reportes/reportes.component';
import { CrearUsuarioComponent } from './usuarios/crear-usuario/crear-usuario.component';
import { VerUsuarioComponent } from './usuarios/ver-usuario/ver-usuario.component';

@NgModule({
  declarations: [
    DashboardComponent,
    NavbarComponent,
    InicioComponent,
    UsuariosComponent,
    ReportesComponent,
    CrearUsuarioComponent,
    VerUsuarioComponent
  ],
  imports: [
    CommonModule,
    DashboardroutingModule,
    SharedModule
  ]
})
export class DashboardModule { }
