import { MenuService } from 'src/app/services/menu.service';
import { MenuI } from 'src/app/interfaces/menu.interface';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  menu: MenuI[] = [];

  constructor(private _menuService: MenuService) { }

  cargarMenu(): void{
    this._menuService.getMenu().subscribe(data => {
      this.menu = data;
    });
  }

  ngOnInit(): void {
    this.cargarMenu();
  }

}
