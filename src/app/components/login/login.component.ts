import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UsuarioI } from 'src/app/interfaces/usuario.interface';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  form!: FormGroup;
  loading: boolean = false

  constructor(private fb: FormBuilder, private _snackbar: MatSnackBar, private router: Router) {
    this.formulario()
  }

  formulario(): void {
    this.form = this.fb.group({
      usuario: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  ingresar() {
    //console.log(this.form);

    const Usuario: UsuarioI = {
      usuario: this.form.value.usuario,
      password: this.form.value.password
    }
  
    console.log(Usuario);

    if(Usuario.usuario === 'Charly' && Usuario.password === 'admin123'){
      //Redireccionamos al dashboard
      console.log("INGRESO");
      
      this.fakeLoading();
    } else {
      this.error();
      this.form.reset();
    }
  }

  fakeLoading(): void {
    this.loading = true;
      //Redireccionamos al dashboard
    setTimeout(() => {
      this.loading = false;
      this.router.navigate(['dashboard']);
    }, 1500);
  }
    
  error():void {
    this._snackbar.open('usuario o contraseña incorrecto', '', {
      duration: 5000,
      horizontalPosition: 'center',
      verticalPosition: 'bottom',
    }); 
  }

  ngOnInit(): void {
  }

}
