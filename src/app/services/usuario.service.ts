import { Injectable } from '@angular/core';
import { UsuarioDataI, listUsuarios } from '../interfaces/usuario.interface';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  listUsuarios: UsuarioDataI[]= listUsuarios

  constructor() { }
  getUsuario(): UsuarioDataI[] {
    // slice retorna una copia del array
    return this.listUsuarios.slice();
  }

  eliminarUsuario(usuario: string){
    this.listUsuarios = this.listUsuarios.filter(data => {
      return data.usuario!== usuario;
    });
  }

  agregarUsuario(usuario: UsuarioDataI) {
    this.listUsuarios.unshift(usuario);
  }

  buscarUsuario(id: string): UsuarioDataI{
    //o retorna un json {} vacio
    return this.listUsuarios.find(element => element.usuario === id) || {} as UsuarioDataI;
  }

  modificarUsuario(user: UsuarioDataI){
    this.eliminarUsuario(user.usuario);
    this.agregarUsuario(user);
  }

}
